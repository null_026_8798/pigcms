<?php
require_once("weixinchat.class.php");

class WeixinAction extends Action
{
    private $token;
    private $fun;
    private $data = array();
    private $my = '小猪猪';

    public function index()
    {
        $this->token = $this->_get('token');
        $weixin = new Wechat($this->token);
        $data = $weixin->request();
        $this->data = $weixin->request();
        $this->my = C('site_my');
        $open = M('Token_open')->where(array(
            'token' => $this->_get('token')
        ))->find();
        $this->fun = $open['queryname'];
        list($content, $type) = $this->reply($data);
        $weixin->response($content, $type);
    }



    function error_msg($data)
    {
        return '没有找到' . $data . '相关的数据';
    }

    public function user($action, $keyword = '')
    {
        $user = M('Wxuser')->field('uid')->where(array(
            'token' => $this->token
        ))->find();
        $usersdata = M('Users');
        $dataarray = array(
            'id' => $user['uid']
        );
        $users = $usersdata->field('gid,diynum,connectnum,activitynum,viptime')->where(array(
            'id' => $user['uid']
        ))->find();
        $group = M('User_group')->where(array(
            'id' => $users['gid']
        ))->find();
        if ($users['diynum'] < $group['diynum'])
        {
            $data['diynum'] = 1;
            if ($action == 'diynum')
            {
                $usersdata->where($dataarray)->setInc('diynum');
            }
        }
        if ($users['connectnum'] < $group['connectnum'])
        {
            $data['connectnum'] = 1;
            if ($action == 'connectnum')
            {
                $usersdata->where($dataarray)->setInc('connectnum');
            }
        }
        if ($users['viptime'] > time())
        {
            $data['viptime'] = 1;
        }
        return $data;
    }

    public function requestdata($field)
    {
        $data['year'] = date('Y');
        $data['month'] = date('m');
        $data['day'] = date('d');
        $data['token'] = $this->token;
        $mysql = M('Requestdata');
        $check = $mysql->field('id')->where($data)->find();
        if ($check == false)
        {
            $data['time'] = time();
            $data[$field] = 1;
            $mysql->add($data);
        } else
        {
            $mysql->where($data)->setInc($field);
        }
    }

    public function behaviordata($field, $id = '', $type = '')
    {
        $data['date'] = date('Y-m-d', time());
        $data['token'] = $this->token;
        $data['openid'] = $this->data['FromUserName'];
        $data['keyword'] = $this->data['Content'];
        $data['model'] = $field;
        if ($id != false)
        {
            $data['fid'] = $id;
        }
        if ($type != false)
        {
            $data['type'] = 1;
        }
        $mysql = M('Behavior');
        $check = $mysql->field('id')->where($data)->find();
        $this->updateMemberEndTime($data['openid']);
        if ($check == false)
        {
            $data['enddate'] = time();
            $mysql->add($data);
        } else
        {
            $mysql->where($data)->setInc('num');
        }
    }

    function updateMemberEndTime($openid)
    {
        $mysql = M('Wehcat_member_enddate');
        $id = $mysql->field('id')->where(array(
            'openid' => $openid
        ))->find();
        $data['enddate'] = time();
        $data['openid'] = $openid;
        if ($id == false)
        {
            $mysql->add($data);
        } else
        {
            $data['id'] = $id;
            $mysql->save($data);
        }
    }



    public function api_notice_increment($url, $data)
    {
        $ch = curl_init();
        $header = "Accept-Charset: utf-8";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        if (curl_errno($ch))
        {
            return false;
        } else
        {
            return $tmpInfo;
        }
    }

    function httpGetRequest_baike($url)
    {
        $headers = array(
            "User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:14.0) Gecko/20100101 Firefox/14.0.1",
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Language: en-us,en;q=0.5",
            "Referer: http://www.baidu.com/"
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);
        if ($output === FALSE)
        {
            return "cURL Error: " . curl_error($ch);
        }
        return $output;
    }

    public function get_tags($title, $num = 10)
    {
        vendor('Pscws.Pscws4', '', '.class.php');
        $pscws = new PSCWS4();
        $pscws->set_dict(CONF_PATH . 'etc/dict.utf8.xdb');
        $pscws->set_rule(CONF_PATH . 'etc/rules.utf8.ini');
        $pscws->set_ignore(true);
        $pscws->send_text($title);
        $words = $pscws->get_tops($num);
        $pscws->close();
        $tags = array();
        foreach ($words as $val)
        {
            $tags[] = $val['word'];
        }
        return implode(',', $tags);
    }
}

?>