<?php

class MapAction extends Action
{
    public $token;
    public $apikey;

    public function _initialize()
    {
        $this->token = $this->_get('token');
        $this->assign('token', $this->token);
        $this->apikey = C('baidu_map_api');
        $this->assign('apikey', $this->apikey);
    }

    //公司静态地图
    public function staticCompanyMap()
    {
        //main company
        $company_model = M('Company');
        $where = array('token' => $this->token);
        $thisCompany = $company_model->where($where)->order('isbranch ASC')->find();
        //branches
        $where['isbranch'] = 1;
        $companies = $company_model->where($where)->order('taxis ASC')->select();
        //
        $return = array();
        $imgUrl = 'http://api.map.baidu.com/staticimage?center=' . $thisCompany['longitude'] . ',' . $thisCompany['latitude'] . '&width=640&height=320&zoom=11&markers=' . $thisCompany['longitude'] . ',' . $thisCompany['latitude'] . '&markerStyles=l,1';
        $titleStr = $thisCompany['name'] . '地图';
        if ($companies)
        {
            $titleStr = '1.' . $titleStr;
        }
        $return[] = array($titleStr, "电话：" . $thisCompany['tel'] . "\r\n地址：" . $thisCompany['address'] . "\r\n回复“开车去”“步行去”或“坐公交”获取详细线路\r\n点击查看详细", $imgUrl, C('site_url') . '/index.php?g=Wap&m=Company&a=map&token=' . $this->token);

        if ($companies)
        {
            $i = 2;
            $sep = '';
            foreach ($companies as $thisCompany)
            {
                $imgUrl = 'http://api.map.baidu.com/staticimage?center=' . $thisCompany['longitude'] . ',' . $thisCompany['latitude'] . '&width=80&height=80&zoom=11&markers=' . $thisCompany['longitude'] . ',' . $thisCompany['latitude'] . '&markerStyles=l,' . $i;
                $return[] = array($i . '.' . $thisCompany['name'] . '地图', "电话：" . $thisCompany['tel'] . "\r\n地址：" . $thisCompany['address'] . "\r\n点击查看详细", $imgUrl, C('site_url') . '/index.php?g=Wap&m=Company&a=map&companyid=' . $thisCompany['id'] . '&token=' . $this->token);
                $i++;
            }
            //使用操作
            $imgUrl = $thisCompany['logourl'];
            $return[] = array('回复“最近的”查看哪一个离你最近，或者回复“开车去+编号”“步行去+编号”或“坐公交+编号”获取详细线路', "电话：" . $thisCompany['tel'] . "\r\n地址：" . $thisCompany['address'] . "\r\n点击查看详细", $imgUrl, C('site_url') . '/index.php?g=Wap&m=Company&a=map&token=' . $this->token);
        }

        return array($return, 'news');
    }



    public function _getTime($duration)
    {
        $duration = $duration / 60;
        if ($duration > 60)
        {
            $durationStr = intval($duration / 100) . '小时';
            if ($duration % 60 > 0)
            {
                $durationStr .= ($duration % 60) . '分钟';
            }
        } else
        {
            $durationStr = intval($duration) . '分钟';
        }
        return $durationStr;
    }
}


?>